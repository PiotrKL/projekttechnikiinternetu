﻿using ProjektTechnikiInternetu.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjektTechnikiInternetu.Models
{
    public class Book
    {
        [Key]
        public int BookId { get; set; }
        [Display(Name = "Tytuł")]
        [Required]
        public string Title { get; set; }
        [Display(Name = "Autor")]
        [Required]
        public string Author { get; set; }
        [Display(Name = "Kategoria")]
        [Required]
        public BookCategory BookCategory { get; set; }
        [Display(Name = "Cena")]
        [Required]
        public double Price { get; set; }

        // Relationships
        public List<BookInOrder> BookInOrders { get; set; }

        public Book()
        {

        }

    }
}
