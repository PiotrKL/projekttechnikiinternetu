﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjektTechnikiInternetu.Models
{
    public class Order
    {
        [Key]
        [Display(Name = "ID")]
        public int OrderId { get; set; }
        [Display(Name = "E-mail zamawiającego")]
        public string BuyerEmail { get; set; }

        [Display(Name = "Adres dostawy")]
        public string Adress { get; set; }

        [Display(Name = "Wartość zamówienia")]
        public double TotalExpense { get; set; }

        // Relationships
        public List<BookInOrder> BooksInOrder { get; set; }

        public Order()
        {

        }

    }
}
