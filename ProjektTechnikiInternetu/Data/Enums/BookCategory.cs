﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace ProjektTechnikiInternetu.Data.Enums
{
    public enum BookCategory
    {
        [Description("Sciene Fiction")]
        ScieneFiction = 1,

        [Description("Kryminał")]
        CrimeStories,

        [Description("Poezja")]
        Poetry,

        [Description("Popularno-naukowa")]
        PopularScience
    }
}
