﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ProjektTechnikiInternetu.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjektTechnikiInternetu.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<BookInOrder> BookInOrders { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.Entity<BookInOrder>().HasKey(bInO => new
            {
                bInO.BookId,
                bInO.OrderId
            });

            builder.Entity<BookInOrder>().HasOne(bInO => bInO.Book).WithMany(b => b.BookInOrders).HasForeignKey(bInO => bInO.BookId);
            builder.Entity<BookInOrder>().HasOne(bInO => bInO.Order).WithMany(o => o.BooksInOrder).HasForeignKey(bInO => bInO.OrderId);

            base.OnModelCreating(builder);
        }
    }
}
