﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using ProjektTechnikiInternetu.Data.Enums;
using ProjektTechnikiInternetu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjektTechnikiInternetu.Data
{
    public class ApplicationDbInitializer
    {
        public static void Seed(IApplicationBuilder applicationBuilder)
        {
            using (var serviceScope = applicationBuilder.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();

                context.Database.EnsureCreated();

                if (!context.Books.Any())
                {
                    context.Books.AddRange(new List<Book>()
                    {
                        new Book()
                        {
                            Title = "Metro 2033",
                            Author = "Dymitr Gluchovsky",
                            BookCategory = BookCategory.ScieneFiction,
                            Price = 35.40
                        },
                        new Book()
                        {
                            Title = "Paradyzja",
                            Author = "Janusz Zajdel",
                            BookCategory = BookCategory.ScieneFiction,
                            Price = 30.20
                        },
                        new Book()
                        {
                            Title = "1984",
                            Author = "George Orwell",
                            BookCategory = BookCategory.ScieneFiction,
                            Price = 45.30
                        },
                        new Book()
                        {
                            Title = "Pan Tadeusz",
                            Author = "Adam Mickiewicz",
                            BookCategory = BookCategory.Poetry,
                            Price = 25.99
                        },
                        new Book()
                        {
                            Title = "Finansowa Forteca",
                            Author = "Marcin Iwuć",
                            BookCategory = BookCategory.PopularScience,
                            Price = 55.56
                        },
                        new Book()
                        {
                            Title = "Koniec Pieniądza Papierowego",
                            Author = "Roland Baader",
                            BookCategory = BookCategory.PopularScience,
                            Price = 70.12
                        },
                        new Book()
                        {
                            Title = "Inteligentny Inwestor XXI Wieku",
                            Author = "Cezary Głuch",
                            BookCategory = BookCategory.PopularScience,
                            Price = 112.20
                        },
                        new Book()
                        {
                            Title = "Morderstwo w Orient Expressie",
                            Author = "Agata Chrieste",
                            BookCategory = BookCategory.CrimeStories,
                            Price = 15.22
                        },
                        new Book()
                        {
                            Title = "Wiersze Wszystkie",
                            Author = "Czesław Miłosz",
                            BookCategory = BookCategory.Poetry,
                            Price = 54.65
                        },
                        new Book()
                        {
                            Title = "Wiersze Wybrane",
                            Author = "Adam Zagajewski",
                            BookCategory = BookCategory.Poetry,
                            Price = 49.99
                        }
                    });
                    context.SaveChanges();
                }
                if (!context.Orders.Any())
                {
                    context.Orders.AddRange(new List<Order>()
                    {
                        new Order()
                        {
                            BuyerEmail = "adam.kowalski@example.pl",
                            Adress = "ul. Kościuszki 20 40-300 Pcim",
                            TotalExpense = 110.9
                        },
                        new Order()
                        {
                            BuyerEmail = "anna.nowak@example.pl",
                            Adress = "ul. Mickiewicza 15 00-320 Gostyń",
                            TotalExpense = 134.84
                        },
                        new Order()
                        {
                            BuyerEmail = "piotr.milek@example.pl",
                            Adress = "ul. Zielona 10 90-100 Bydgoszcz",
                            TotalExpense = 125.68
                        },
                        new Order()
                        {
                            BuyerEmail = "adam.kowalski@example.pl",
                            Adress = "ul. Kościuszki 20 40-300 Pcim",
                            TotalExpense = 25.99
                        },
                        new Order()
                        {
                            BuyerEmail = "michal.franek@example.pl",
                            Adress = "ul. Dworcowa 1 42-320 Zielona Góra",
                            TotalExpense = 41.21
                        },
                        new Order()
                        {
                            BuyerEmail = "ewelina.wilczynska@example.pl",
                            Adress = "ul. Torowa 9 21-230 Gdańsk",
                            TotalExpense = 99.95
                        },
                        new Order()
                        {
                            BuyerEmail = "anna.nowak@example.pl",
                            Adress = "ul. Mickiewicza 15 00-320 Gostyń",
                            TotalExpense = 90.96
                        },
                    });
                    context.SaveChanges();
                }
                if (!context.BookInOrders.Any())
                {
                    context.BookInOrders.AddRange(new List<BookInOrder>()
                    {
                        new BookInOrder()
                        {
                            BookId = 1,
                            OrderId = 1
                        },
                        new BookInOrder()
                        {
                            BookId = 2,
                            OrderId = 1
                        },
                        new BookInOrder()
                        {
                            BookId = 3,
                            OrderId = 1
                        },
                        new BookInOrder()
                        {
                            BookId = 9,
                            OrderId = 2
                        },
                        new BookInOrder()
                        {
                            BookId = 10,
                            OrderId = 2
                        },
                        new BookInOrder()
                        {
                            BookId = 2,
                            OrderId = 2
                        },
                        new BookInOrder()
                        {
                            BookId = 6,
                            OrderId = 3
                        },
                        new BookInOrder()
                        {
                            BookId = 5,
                            OrderId = 3
                        },
                        new BookInOrder()
                        {
                            BookId = 7,
                            OrderId = 4
                        },
                        new BookInOrder()
                        {
                            BookId = 8,
                            OrderId = 5
                        },
                        new BookInOrder()
                        {
                            BookId = 4,
                            OrderId = 5
                        },
                        new BookInOrder()
                        {
                            BookId = 9,
                            OrderId = 6
                        },
                        new BookInOrder()
                        {
                            BookId = 3,
                            OrderId = 6
                        },
                        new BookInOrder()
                        {
                            BookId = 1,
                            OrderId = 7
                        },
                        new BookInOrder()
                        {
                            BookId = 5,
                            OrderId = 7
                        }
                    });
                    context.SaveChanges();
                }
            }
        }
    }
}
